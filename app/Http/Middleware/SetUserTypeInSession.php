<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SetUserTypeInSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // To execute middleware after request.
        // As Auth provides the current user after login which we are setting in the session.
        $response = $next($request);

        $is_userType_recruiter = Auth::user()->profile['is_recruiter'];         // Fetch the user type from DB.
        $request->session()->put('is_userType_recruiter', $is_userType_recruiter);

        return $response;
    }
}
