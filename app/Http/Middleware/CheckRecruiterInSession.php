<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class CheckRecruiterInSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $is_userType_recruiter = $request->session()->get('is_userType_recruiter');

        // If the user is logged in as Recruiter (i.e. is_userType_recruiter = 1) 
        // and current page is not 'profile' (to stop multiple redirections.)
        // redirecting to homepage of Candidate
        if($is_userType_recruiter != Config::get('constants.userType_recruiter') && $request->url() != route('available_jobs'))
            return redirect(route('available_jobs'));
        return $next($request);
    }
}
