<?php

namespace App\Http\Controllers;

use App\Models\Position;
use App\Models\Experience;
use App\Models\Role;
use App\Models\Company;
use App\Models\User;
use App\Models\Job;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.create_job')->with('companiesArr', Company::all())
            ->with('experienceArr', Experience::all())->with('rolesArr', Role::all())->with('positionsArr', Position::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'title' => 'required | min:5 | max:50 ',
            'description' => 'required | min:8 | max:100',
            'location' => 'required | min:3 | max:15',
            'skill_set' => 'required | max:20',
            'salary' => 'numeric | min:10000 | nullable',
            'position' => 'required',
            'experience' => 'required',
            'role' => 'required',
            'position' => 'required',
        ]);

        $current_user_id = Auth::user()->id; 
        $current_user_company_id = Auth::user()->profile->company->id;
        $wfh_aval = $request->is_wfh_available || FALSE;

        Job::create([
            'title' => $request->title,
            'description' => $request->description,
            'location' => $request->location,
            'skill_set_required' => $request->skill_set,
            'salary' => $request->salary,
            'role_id' => $request->role,
            'position_id' => $request->position,
            'experience_id' => $request->experience,
            'company_id' => $current_user_company_id,
            'created_by' => $current_user_id,
            'is_wfh_available' => $wfh_aval,
            'is_active' => TRUE,
        ]);

        return redirect(route('posted_jobs'));     

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function show(Job $job, $id)
    {
        return view('pages.view_job')->with('jobArr', Job::find($id))->with('companiesArr', Company::all())
            ->with('experienceArr', Experience::all())->with('rolesArr', Role::all())->with('positionsArr', Position::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function get_archive_jobs(Request $request, Job $job)
    {
        $user_id = Auth::user()->id;
        $user_company_id = Auth::user()->profile->company->id;
        return view('pages.archived_jobs')->with('jobsArr', Job::where('is_active',FALSE)
            ->where('company_id', $user_company_id)->where('created_by', $user_id)->get());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function edit(Job $job, $id)
    {
        return view('pages.edit_job')->with('jobArr', Job::find($id))->with('companiesArr', Company::all())
            ->with('experienceArr', Experience::all())->with('rolesArr', Role::all())->with('positionsArr', Position::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Job $job, $id)
    {
        $request->validate([
            'title' => 'required | min:5 | max:50 ',
            'description' => 'required | min:8 | max:100',
            'location' => 'required | min:3 | max:15',
            'skill_set' => 'required | max:8',
            'salary' => 'numeric | min:10000 | nullable',
            'position' => 'required',
            'experience' => 'required',
            'role' => 'required',
            'position' => 'required',
        ]);

        $current_user_id = Auth::user()->id; 
        $current_user_company_id = Auth::user()->profile->company->id;
        $wfh_aval = $request->is_wfh_available || FALSE;
        
        Job::updateOrCreate(
            ['id' => $id],
            [
                'title' => $request->title,
                'description' => $request->description,
                'location' => $request->location,
                'skill_set_required' => $request->skill_set,
                'salary' => $request->salary,
                'role_id' => $request->role,
                'position_id' => $request->position,
                'experience_id' => $request->experience,
                'company_id' => $current_user_company_id,
                'created_by' => $current_user_id,
                'is_wfh_available' => $wfh_aval,
                'is_active' => TRUE,
        ]);

        return redirect(route('posted_jobs'));
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function destroy(Job $job)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function create_job(Job $job)
    {
        //echo Form::model($job, array('route' => array('job.update', $job->id)));
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function posted_jobs(Job $job)
    {
        $user_id = Auth::user()->id;
        $user_company_id = Auth::user()->profile->company->id;

        return view('pages.posted_jobs')->with('jobsArr', Job::where('is_active',TRUE)
            ->where('company_id', $user_company_id)->where('created_by', $user_id)->orderBy('created_at', 'desc')->get());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function view_applications(Job $job, $id)
    {
        return view('pages.view_applications')->with('jobsArr', Job::find($id))->with('usersArr', Job::find($id)->applied_by_users);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function available_jobs(Job $job)
    {
        $current_user = Auth::user();
        
        $jobsArr = Job::where('is_active', TRUE);
        $jobsArr = $jobsArr->where('experience_id', '>=', $current_user->profile->experience_id)->where('experience_id', '<=', $current_user->profile->experience_id + 1);
        $jobsArr = $jobsArr->where('role_id', '=', $current_user->profile->role_id);
        $jobsArr = $jobsArr->where('position_id', '>=', $current_user->profile->position_id)->where('position_id', '<=', $current_user->profile->position_id + 1);
        $jobsArr = $jobsArr->orderBy('created_at', 'desc');


        return view('pages.available_jobs')->with('jobsArr', $jobsArr->get())->with('user', $current_user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function filter_jobs(Job $job)
    {
        $jobsArr = Job::where('is_active', TRUE);
        $current_user = Auth::user();
        $current_profile = $current_user->profile;
        $query_params = request()->query();
        $relative = array_key_exists('relative', $query_params);

        if(empty($query_params)){

        }
        elseif($relative){
            if(array_key_exists('experience', $query_params))
                $jobsArr = $jobsArr->where('experience_id', '>=', $current_profile->experience_id)->where('experience_id', '<=', $current_profile->experience_id + 1);
            
            if(array_key_exists('role', $query_params))
                $jobsArr = $jobsArr->where('role_id', $current_profile->role_id);

            if(array_key_exists('position', $query_params))
                $jobsArr = $jobsArr->where('position_id', '>=', $current_profile->position_id)->where('position_id', '<=', $current_profile->position_id + 1);

            if(array_key_exists('creation', $query_params))
                $jobsArr = $jobsArr->orderBy('created_at', 'desc');

            if(array_key_exists('wfh', $query_params))
                $jobsArr = $jobsArr->where('is_wfh_available', );
        } else {

            if(array_key_exists('experience', $query_params))
                $jobsArr = $jobsArr->orderBy('experience_id','desc');

            if(array_key_exists('role', $query_params))
                $jobsArr = $jobsArr->orderBy('role_id', 'desc');

            if(array_key_exists('position', $query_params))
                $jobsArr = $jobsArr->orderBy('position_id', 'desc');

            if(array_key_exists('creation', $query_params))
                $jobsArr = $jobsArr->orderBy('created_at', 'desc');

            if(array_key_exists('wfh', $query_params))
                $jobsArr = $jobsArr->where('is_wfh_available', TRUE);
        }
        return view('pages.filter_jobs')->with('jobsArr', $jobsArr->get())->with('user', $current_user)->with('query_params', $query_params);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function applied_jobs()//Job $job
    {
        $appliedJobsArr = Auth::user()->applied_jobs;
        return view('pages.applied_jobs')->with('appliedJobsArr', $appliedJobsArr);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function apply_for_job(Request $request, $id)
    {
        $user = Auth::user();
        $job = Job::find($id);

        if( $user->applied_jobs->where('id', $id)->isEmpty()){
            $job->applied_by_users()->attach($user->id);
        } else {
            echo "Already present";       // Display msg
        }
        
        return redirect($request->header('referer'));
    }

    public function remove_job(Request $request, $id)
    {
        $user = Auth::user();
        $job = Job::find($id);

        if( ! $user->applied_jobs->where('id', $id)->isEmpty()){
            $job->applied_by_users()->detach($user->id);
        } else {
            return "Not present";       // Display msg
        }
        
        return redirect($request->header('referer'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function archive_job_operation(Job $job, $id)
    {
        Job::where('id','=',$id)->update(array('is_active' => FALSE));

        return redirect('posted_jobs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function unarchive_job_operation(Job $job, $id)
    {
        Job::where('id','=',$id)->update(array('is_active' => TRUE));

        return redirect('archived_jobs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function search_job(Job $job, $id)
    {
        Job::where('id','=',$id)->update(array('is_active' => TRUE));

        return redirect('archived_jobs');
    }
}
