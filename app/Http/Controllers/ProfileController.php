<?php

namespace App\Http\Controllers;

use App\Models\Position;
use App\Models\Experience;
use App\Models\Role;
use App\Models\Company;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.view_profile_details')->with('user', Auth::user())->with('companiesArr', Company::all())
            ->with('experienceArr', Experience::all())->with('rolesArr', Role::all())->with('positionsArr', Position::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.create_profile_details')->with('auth_user', Auth::user())->with('companiesArr', Company::all())
            ->with('experienceArr', Experience::all())->with('rolesArr', Role::all())->with('positionsArr', Position::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required | min:5 | max:25 ',
            'email' => 'required | email',
            'company' => 'required',
            'position' => 'required',
            'experience' => 'required',
            'role' => 'required',
            'skill_set' => 'max:20 | required',
            'bio' => 'max:80 | required',
        ]);

        $current_user_id = Auth::user()->id;
        $prefer_wfh = $request->prefer_wfh ? TRUE : FALSE;
        $is_recruiter = $request->is_recruiter ? TRUE : FALSE;
        
        $request->session()->put('is_userType_recruiter', $is_recruiter);
        User::updateOrCreate(
            ['id' => $current_user_id],
            [
            'name' => $request->name,
            'email' => $request->email,
        ]);
        
        Profile::updateOrCreate(
            ['user_id' => $current_user_id],
            [
            'company_id' => $request->company,
            'position_id' => $request->position,
            'experience_id' => $request->experience,
            'role_id' => $request->role,
            'skill_set' => $request->skill_set,
            'bio' => $request->bio,
            'prefer_wfh' => $prefer_wfh,
            'is_recruiter' => $is_recruiter,
        ]);
        return redirect(route('profile'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Profile $profile, $id)
    {
        // $is_recruiter = $request->session()->get('is_userType_recruiter');
        return view('pages.view_profile_details')->with('user', User::find($id))->with('companiesArr', Company::all())
            ->with('experienceArr', Experience::all())->with('rolesArr', Role::all())->with('positionsArr', Position::all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit(Profile $profile)
    {
        return view('pages.edit_profile_details')->with('auth_user', Auth::user())->with('companiesArr', Company::all())
            ->with('experienceArr', Experience::all())->with('rolesArr', Role::all())->with('positionsArr', Position::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile, $id)
    {
        $request->validate([
            'name' => 'required | min:5 | max:25 ',
            'email' => 'required | email',
            'company' => 'required',
            'position' => 'required',
            'experience' => 'required',
            'role' => 'required',
            'skill_set' => 'max:20 | required',
            'bio' => 'max:80 | required',
        ]);

        $current_user_id = Auth::user()->id;
        $prefer_wfh = $request->prefer_wfh ? TRUE : FALSE;
        $is_recruiter = $request->is_recruiter ? TRUE : FALSE;
        $request->session()->put('is_userType_recruiter', $is_recruiter);
        
        User::updateOrCreate(
            ['id' => $current_user_id],
            [
            'name' => $request->name,
            'email' => $request->email,
        ]);
        
        Profile::updateOrCreate(
            ['user_id' => $current_user_id],
            [
            'company_id' => $request->company,
            'position_id' => $request->position,
            'experience_id' => $request->experience,
            'role_id' => $request->role,
            'skill_set' => $request->skill_set,
            'bio' => $request->bio,
            'prefer_wfh' => $prefer_wfh,
            'is_recruiter' => $is_recruiter,
        ]);
        return redirect(route('profile'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        //
    }
}
