<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;
    protected $table = 'profiles';

    protected $fillable = ['company_id', 'experience_id', 'position_id', 'role_id', 'user_id', 'skill_set', 'bio', 'prefer_wfh', 'is_recruiter'];

    /**
     * Get the user of this profile.
     */
    public function user()
    {
        return $this->belongsTo('App\models\User');
    }

    /**
     * Get the user of this profile.
     */
    public function company()
    {
        return $this->belongsTo('App\models\Company');
    }

    public function experience()
    {
        return $this->belongsTo('App\Models\Experience');
    }

    public function position()
    {
        return $this->belongsTo('App\Models\Position');
    }

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }
}
