<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;
    protected $table = 'companies';

    /**
     * Get the user of this profile.
     */
    public function jobs()
    {
        return $this->hasMany('App\models\Job');
    }

    /**
     * Get the user of this profile.
     */
    public function profile()
    {
        return $this->hasOne('App\models\Profile');
    }
}
