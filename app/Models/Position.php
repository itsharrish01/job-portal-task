<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    use HasFactory;
    protected $table = 'positions';

    public function job()
    {
        return $this->hasOne('App\Models\Job');
    }

    public function profile()
    {
        return $this->hasOne('App\Models\Profile');
    }
}
