<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    use HasFactory;
    protected $table = 'jobs';

    protected $fillable = ['title', 'description', 'location', 'skill_set_required', 'salary', 'experience_id', 'role_id', 'position_id', 'is_wfh_available', 'company_id', 'created_by', 'is_active'];

    /**
     * Get the user of this profile.
     */
    public function company()
    {
        return $this->belongsTo('App\models\Company');
    }

    /**
     * Get the user of this profile.
     */
    public function user()
    {
        return $this->belongsTo('App\models\User', 'created_by');
    }

    /**
     * Get the profile associated with this user.
     */
    public function applied_by_users()
    {
        return $this->belongsToMany('App\Models\User');
    }

    public function experience()
    {
        return $this->belongsTo('App\Models\Experience');
    }

    public function position()
    {
        return $this->belongsTo('App\Models\Position');
    }

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }
}
