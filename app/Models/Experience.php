<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    use HasFactory;
    protected $table = 'experiences';

    public function job()
    {
        return $this->hasOne('App\Models\Job');
    }

    public function profile()
    {
        return $this->hasOne('App\Models\Profile');
    }
}
