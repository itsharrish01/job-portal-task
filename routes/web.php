<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['auth']], function() {

    Route::post('/edit_profile_operation/{id}', 'App\Http\Controllers\ProfileController@update')->name('edit_profile_operation');
    Route::get('/profile', 'App\Http\Controllers\ProfileController@index')->middleware('is_profile_created_else_redirect')->name('profile');
    Route::get('/view_profile/{id}', 'App\Http\Controllers\ProfileController@show')->name('view_profile');
    Route::get('/edit_profile/{id}', 'App\Http\Controllers\ProfileController@edit')->name('edit_profile');
    Route::get('/create_profile', 'App\Http\Controllers\ProfileController@create')->name('create_profile');
    Route::post('/create_profile_operation', 'App\Http\Controllers\ProfileController@store')->name('create_profile_operation');
    Route::get('/view_job/{id}', 'App\Http\Controllers\JobController@show')->name('view_job');

    Route::group(['middleware' => ['is_user_recruiter_else_redirect']], function() {
        // For Recruiter Login.
        // Disabling candidates to access Recruiter's routes through hard-coded URLs using 'is_user_recruiter_else_redirect' middleware.
        Route::get('/archived_jobs', 'App\Http\Controllers\JobController@get_archive_jobs')->name('archived_jobs');
        Route::get('/create_job', 'App\Http\Controllers\JobController@create')->name('create_job');
        Route::post('/create_job_op', 'App\Http\Controllers\JobController@store')->name('create_job_operation');
        Route::get('/posted_jobs', 'App\Http\Controllers\JobController@posted_jobs')->name('posted_jobs');
        Route::get('/view_applications/{id}', 'App\Http\Controllers\JobController@view_applications')->name('view_applications'); //

        // For CRUD operations - Recruiter.
        Route::get('/archive_job/{id}', 'App\Http\Controllers\JobController@archive_job_operation')->name('archive_job');
        Route::get('/unarchive_job/{id}', 'App\Http\Controllers\JobController@unarchive_job_operation');
        Route::get('/edit_job/{id}', 'App\Http\Controllers\JobController@edit')->name('edit_job');
        Route::post('/edit_job_op/{id}', 'App\Http\Controllers\JobController@update')->name('edit_job_operation');
    });

    Route::group(['middleware' => ['is_user_candidate_else_redirect']], function() {
        // For Candidate Login.
        // Disabling recruiters to access Candidate's routes through hard-coded URLs using 'is_user_candidate_else_redirect' middleware.
        Route::get('/applied_jobs', 'App\Http\Controllers\JobController@applied_jobs')->name('applied_jobs'); //
        Route::get('/available_jobs', 'App\Http\Controllers\JobController@available_jobs')->name('available_jobs'); //
        Route::get('/filter_jobs', 'App\Http\Controllers\JobController@filter_jobs')->name('filter_jobs'); //
    
        // For CRUD operations - Candidate.
        Route::get('/apply_for_job/{id}', 'App\Http\Controllers\JobController@apply_for_job')->name('apply_for_job');
        Route::get('/remove_job/{id}', 'App\Http\Controllers\JobController@remove_job')->name('remove_job');
    });
    
});

// Home page - needs no authentication.
Route::get('/', function () {  return view('welcome'); });

require __DIR__.'/auth.php';
