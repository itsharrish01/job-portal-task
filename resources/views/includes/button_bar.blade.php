<div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">

    @php
    $userType_config = config()->get('constants.userType_recruiter');
    @endphp

    @if(session()->has('is_userType_recruiter') and session()->get('is_userType_recruiter') == $userType_config)
        <h2>Recruiter's Dashboard</h2>
        
        <a href="{{ route('create_job') }}"> <button type="button" class="btn btn-outline-secondary">Create New Job</button></a>
        
        <a href="{{ route('posted_jobs') }}"> <button type="button" class="btn btn-outline-success">Posted Jobs</button></a>
        
        <a href="{{ route('archived_jobs') }}"><button type="button" class="btn btn-outline-danger">Archived Jobs</button></a>

        <a href="{{ route('profile') }}"> <button type="button" class="btn btn-outline-primary">Your Profile</button></a>

    @else
        <h2>Candidate's Dashboard</h2>
        
        <a href="{{ route('available_jobs') }}"> <button type="button" class="btn btn-outline-secondary">Available Jobs</button></a>
        
        <a href="{{ route('applied_jobs') }}"> <button type="button" class="btn btn-outline-success">Applied Jobs</button></a>
        
        <a href="{{ route('filter_jobs') }}"><button type="button" class="btn btn-outline-danger">Filter Jobs Page</button></a>
    
        <a href="{{ route('profile') }}"> <button type="button" class="btn btn-outline-primary">Your Profile</button></a>
    @endif
</div>