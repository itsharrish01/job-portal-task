@extends('layouts.default')

@section('main-content')

<p class="h4">View Applicants for the job you posted:</p>

Job details:</br>
Job id => {{ $jobsArr->id }}</br>
Job title => {{ $jobsArr->title }}</br>

@if(! $usersArr->isEmpty())
<table class="table table-dark table-striped">
    <thead>
        <tr>
        <th scope="col">Candidate Name</th>
        <th scope="col">Candidate Email</th>
        <th scope="col">Current Company</th>
        <th scope="col">Current Position</th>
        <th scope="col">Current Role</th>
        <th scope="col">WFH availability</th>
        </tr>
    </thead>
    <tbody>
        @foreach($usersArr as $user)
            <th scope="row">
            <a href="{{ route('view_profile', $user->id) }}" style="color: inherit; text-decoration: none;"> {{ $user->name }} </a>
            </th>
            <td>
            <a href="{{ route('view_profile', $user->id) }}" style="color: inherit; text-decoration: none;"> {{ $user->email }} </a>
            </td>
            <td>{{ $user->profile->company->name }}</td>
            <td>{{ $user->profile->position->name }}</td>
            <td>{{ $user->profile->role->name }}</td>
            <td>{{ $user->profile->experience->name }}</td>
            
        @endforeach
    </tbody>
</table>
@else
<p class="h5">**** Currently no candidate has applied for this Job Profile ****</p>
@endif

@endsection