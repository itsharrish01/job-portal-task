@extends('layouts.default')

@section('main-content')

<p class="h4">Create a new Job:</p>

<form  action="{{ route('create_job_operation') }}" method="POST">

    <!-- CROSS Site Request Forgery Protection -->
    @csrf

    <div class="form-group">
        <label>Job Title</label>
        <input type="text" class="form-control" name="title" id="title">
        <span class="field-error">
            @error('title')
                {{ $message }}
            @enderror
        </span>
    </div></br>

    <div class="form-group">
        <label>Description</label>
        <textarea class="form-control" name="description" id="description" rows="4"></textarea>
        <span class="field-error">
            @error('description')
                {{ $message }}
            @enderror
        </span>
    </div></br>

    <div class="form-group">
        <label>Job Location</label>
        <input type="text" class="form-control" name="location" id="location">
        <span class="field-error">
            @error('location')
                {{ $message }}
            @enderror
        </span>
    </div></br>

    <div class="form-group">
        <label>CTC Offered</label>
        <input type="text" class="form-control" name="salary" id="salary">
        <span class="field-error">
            @error('salary')
                {{ $message }}
            @enderror
        </span>
    </div></br>

    <div class="form-group">
        <label>Required Skill set</label>
        <textarea class="form-control" name="skill_set" id="skill_set" rows="4"></textarea>
        <span class="field-error">
            @error('skill_set')
                {{ $message }}
            @enderror
        </span>
    </div></br>

    <div class="form-group">
        <label>Required Position</label>
        <select class="form-control" name="position" id="position">
            <option value="">--</option>
            @foreach($positionsArr as $position)
                <option value="{{ $position->id }}">{{ $position->name }}</option>
            @endforeach
        </select>
        <span class="field-error">
            @error('position')
                {{ $message }}
            @enderror
        </span>
    </div></br>

    <div class="form-group">
        <label>Required Experience</label>
        <select class="form-control" name="experience" id="experience">
            <option value="">--</option>
            @foreach($experienceArr as $experience)
                <option value="{{ $experience->id }}">{{ $experience->name }}</option>
            @endforeach
        </select>
        <span class="field-error">
            @error('experience')
                {{ $message }}
            @enderror
        </span>
    </div></br>

    <div class="form-group">
        <label>Required Role</label>
        <select class="form-control" name="role" id="role">
            <option value="">--</option>
            @foreach($rolesArr as $role)
                <option value="{{ $role->id }}">{{ $role->name }}</option>
            @endforeach
        </select>
        <span class="field-error">
            @error('role')
                {{ $message }}
            @enderror
        </span>
    </div></br>

    <div class="form-group form-check">
        <input type="checkbox" class="form-check-input" name="is_wfh_available" id="is_wfh_available">
        <label>WFH Availability</label>
    </div></br>

    <input type="submit" name="send" value="Submit" class="btn btn-dark btn-block">
</form>
</br>

<style>
    .field-error{color:red;}
</style>

@endsection