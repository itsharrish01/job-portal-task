@extends('layouts.default')

@section('main-content')
  <div class="d-grid gap-2 col-6 mx-auto">
    <button class="btn btn-primary" type="button">
      <a href="{{ route('create_job') }}" style="color: inherit; text-decoration: none;">
        Post a new Job
      </a>
    </button>
  </div>


  <p class="h4">Jobs posted by you:</p>
  <table class="table table-dark table-striped">
    <thead>
      <tr>
        <th scope="col">Id.</th>
        <th scope="col">Title</th>
        <th scope="col">Location</th>
        <th scope="col">Offering CTC</th>
        <th scope="col">Company</th>
        <th scope="col">Required Exp</th>
        <th scope="col">Required Role</th>
        <th scope="col">Required Position</th>
        <th scope="col">Actions</th>
        <th scope="col"></th>
        <th scope="col"></th>
      </tr>
    </thead>
    <tbody>
      @foreach($jobsArr as $job)
      <tr>
        <th scope="row">
          <a href="{{ route('view_job', $job->user->id) }}" style="color: inherit; text-decoration: none;"> {{ $job->id }} </a>
        </th>
        <td>
          <a href="{{ route('view_job', $job->user->id) }}" style="color: inherit; text-decoration: none;"> {{ $job->title }} </a>
        </td>
        <td>{{ $job->location }}</td>
        <td>Rs. {{ $job->salary }}</td>
        <td>{{ $job->company->name }}</td>
        <td>{{ $job->experience->name }}</td>
        <td>{{ $job->role->name }}</td>
        <td>{{ $job->position->name }}</td>
        <td><a href="{{ route('edit_job', $job->id) }}"><button type="button" class="btn btn-light">Edit</button></a></td>
        <td><a href="{{ route('archive_job', $job->id) }}"><button type="button" class="btn btn-light">Archive</button></a></td>
        <td><a href="{{ route('view_applications', $job->id) }}"><button type="button" class="btn btn-light">View Applications</button></a></td>
      </tr>
      @endforeach
    </tbody>
  </table>

@endsection