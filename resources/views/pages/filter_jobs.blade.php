@extends('layouts.default')

@section('main-content')

<p class="h4">Filter through Jobs:</p></br>

<form>
<table class="table table-dark table-striped">
  <thead>
    <tr>
      <th scope="col">Filter by:</th>
      <th scope="col">
        <input type="checkbox" id="relative" name="relative" value="1" {{ (array_key_exists('relative', $query_params))? "checked": ""}}>
        <label for="relative">Self-Relatively</label>
      </th>
      <th scope="col">
        <input type="checkbox" id="experience" name="experience" value="1" {{ (array_key_exists('experience', $query_params))? "checked": ""}}>
        <label for="experience">Experience</label></th>
      <th scope="col">
        <input type="checkbox" id="role" name="role" value="1" {{ (array_key_exists('role', $query_params))? "checked": ""}}>
        <label for="role">Role</label></th>
      <th scope="col">
        <input type="checkbox" id="position" name="position" value="1" {{ (array_key_exists('position', $query_params))? "checked": ""}}>
        <label for="position">Position</label></th>
      <th scope="col">
        <input type="checkbox" id="creation" name="creation" value="1" {{ (array_key_exists('creation', $query_params))? "checked": ""}}>
        <label for="creation">Latest</label></th>
      <th scope="col">
        <input type="checkbox" id="wfh" name="wfh" value="1" {{ (array_key_exists('wfh', $query_params))? "checked": ""}}>
        <label for="wfh">WFH</label></th>
      <th scope="col">
        <input type="submit">
    </tr>
  </thead>
</table>
</form>

</br>
<table class="table table-dark table-striped">
    <thead>
      <tr>
        <th scope="col">Id.</th>
        <th scope="col">Title</th>
        <th scope="col">Location</th>
        <th scope="col">Offering CTC</th>
        <th scope="col">Company</th>
        <th scope="col">Required Exp</th>
        <th scope="col">Required Role</th>
        <th scope="col">Required Position</th>
        <th scope="col">Created by</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
      @foreach($jobsArr as $job)
      <tr>
        <th scope="row">
          <a href="{{ route('view_job', $job->id) }}" style="color: inherit; text-decoration: none;"> {{ $job->id }} </a>
        </th>
        <td>
          <a href="{{ route('view_job', $job->id) }}" style="color: inherit; text-decoration: none;"> {{ $job->title }} </a>
        </td>
        <td>{{ $job->location }}</td>
        <td>Rs. {{ $job->salary }}</td>
        <td>{{ $job->company->name }}</td>
        <td>{{ $job->experience->name }}</td>
        <td>{{ $job->role->name }}</td>
        <td>{{ $job->position->name }}</td>
        <td>
            <a href="{{ route('view_profile', $job->user->id) }}" style="color: inherit; text-decoration: none;"> {{ $job->user->name }} </a>
        </td>
        @if( $user->applied_jobs->where('id', $job->id)->isEmpty() )
          <td><a href="{{ route('apply_for_job', $job->id) }}"><button type="button" class="btn btn-light">Apply</button></a></td>
        @else
          <td><button type="button" class="btn btn-light" disabled>Applied</button></td>
        @endif
      </tr>
      @endforeach
    </tbody>
  </table>

@endsection