@extends('layouts.default')

@section('main-content')


<p class="h4">Your Profile Details:</p>
<form  action="{{ route('edit_profile_operation', [$auth_user->id]) }}" method="POST">

    <!-- CROSS Site Request Forgery Protection -->
    @csrf

    <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control" name="name" id="name" value="{{ $auth_user->name}}">
        <span class="field-error">
            @error('name')
                {{ $message }}
            @enderror
        </span>
    </div></br>    

    <div class="form-group">
        <label>Email</label>
        <input type="text" class="form-control" name="email" id="email" value="{{ $auth_user->email}}">
        <span class="field-error">
            @error('email')
                {{ $message }}
            @enderror
        </span>
    </div></br>

    <div class="form-group">
        <label>Company</label>
        <select class="form-control" name="company" id="company">
            <option value="">--</option>
            @foreach($companiesArr as $company)
                <option value="{{ $company->id }}" {{ ($company->id == $auth_user->profile->company_id) ? "selected" : "" }}>{{ $company->name }}</option>
            @endforeach
        </select>
        <span class="field-error">
            @error('company')
                {{ $message }}
            @enderror
        </span>
    </div></br>

    <div class="form-group">
        <label>Position</label>
        <select class="form-control" name="position" id="position">
            <option value="">--</option>
            @foreach($positionsArr as $position)
                <option value="{{ $position->id }}" {{ ($position->id == $auth_user->profile->position_id) ? "selected" : "" }}>{{ $position->name }}</option>
            @endforeach
        </select>
        <span class="field-error">
            @error('position')
                {{ $message }}
            @enderror
        </span>
    </div></br>

    <div class="form-group">
        <label>Experience</label>
        <select class="form-control" name="experience" id="experience">
            <option value="">--</option>
            @foreach($experienceArr as $experience)
                <option value="{{ $experience->id }}" {{ ($experience->id == $auth_user->profile->experience_id) ? "selected" : "" }}>{{ $experience->name }}</option>
            @endforeach
        </select>
        <span class="field-error">
            @error('experience')
                {{ $message }}
            @enderror
        </span>
    </div></br>

    <div class="form-group">
        <label>Role</label>
        <select class="form-control" name="role" id="role">
            <option value="">--</option>
            @foreach($rolesArr as $role)
                <option value="{{ $role->id }}" {{ ($role->id == $auth_user->profile->role_id) ? "selected" : "" }}>{{ $role->name }}</option>
            @endforeach
        </select>
        <span class="field-error">
            @error('role')
                {{ $message }}
            @enderror
        </span>
    </div></br>
    
    <div class="form-group">
        <label>Skill set</label>
        <textarea class="form-control" name="skill_set" id="skill_set" rows="4">{{ $auth_user->profile->skill_set }}</textarea>
        <span class="field-error">
            @error('skill_set')
                {{ $message }}
            @enderror
        </span>
    </div></br>

    <div class="form-group">
        <label>Brief Bio.</label>
        <textarea class="form-control" name="bio" id="bio" rows="4">{{ $auth_user->profile->bio }}</textarea>
        <span class="field-error">
            @error('bio')
                {{ $message }}
            @enderror
        </span>
    </div></br>

    <div class="form-group form-check">
        <input type="checkbox" class="form-check-input" name="prefer_wfh" id="prefer_wfh" {{ $auth_user->profile->prefer_wfh ? 'checked' : "" }}>
        <label>Prefer WFH?</label>
    </div>

    <div class="form-group form-check">
        <input type="checkbox" class="form-check-input" name="is_recruiter" id="is_recruiter" {{ $auth_user->profile->is_recruiter ? 'checked' : "" }}>
        <label>Is Recruiter?</label>
    </div>
    
    <input type="submit" name="Submit details" value="Submit" class="btn btn-dark btn-block">
</form>
</br>

<style>
    .field-error{color:red;}
</style>

@endsection