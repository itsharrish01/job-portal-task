@extends('layouts.default')

@section('main-content')


<p class="h4">Your Profile Details:</p>
<div>

    <!-- CROSS Site Request Forgery Protection -->
    @csrf

    <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control" name="name" id="name" value="{{ $user->name}}" readonly>
    </div></br>    

    @if( request()->path() == 'profile' )
        <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control" name="email" id="email" value="{{ $user->email}}" readonly>
        </div></br>
    @endif

    <div class="form-group">
        <label>Company</label>
        <select class="form-control" name="company" id="company" disabled>
            <option value="">--</option>
            @foreach($companiesArr as $company)
                <option value="{{ $company->id }}" {{ ($company->id == $user->profile->company_id) ? "selected" : "" }}>{{ $company->name }}</option>
            @endforeach
        </select>
    </div></br>

    <div class="form-group">
        <label>Position</label>
        <select class="form-control" name="position" id="position" disabled>
            @foreach($positionsArr as $position)
                <option value="{{ $position->id }}" {{ ($position->id == $user->profile->position_id) ? "selected" : "" }}>{{ $position->name }}</option>
            @endforeach
        </select>
    </div></br>

    <div class="form-group">
        <label>Experience</label>
        <select class="form-control" name="experience" id="experience" disabled>
            <option value="">--</option>
            @foreach($experienceArr as $experience)
                <option value="{{ $experience->id }}" {{ ($experience->id == $user->profile->experience_id) ? "selected" : "" }}>{{ $experience->name }}</option>
            @endforeach
        </select>
    </div></br>

    <div class="form-group">
        <label>Role</label>
        <select class="form-control" name="role" id="role" disabled>
            <option value="">--</option>
            @foreach($rolesArr as $role)
                <option value="{{ $role->id }}" {{ ($role->id == $user->profile->role_id) ? "selected" : "" }}>{{ $role->name }}</option>
            @endforeach
        </select>
    </div></br>
    
    <div class="form-group">
        <label>Skill set</label>
        <textarea class="form-control" name="skill_set" id="skill_set" rows="4" readonly>{{ $user->profile->skill_set }}</textarea>
    </div></br>

    <div class="form-group">
        <label>Brief Bio.</label>
        <textarea class="form-control" name="bio" id="bio" rows="4" readonly>{{ $user->profile->bio }}</textarea>
    </div></br>

    @if( request()->path() == 'profile' )
        <div class="form-group form-check">
            <input type="checkbox" class="form-check-input" name="prefer_wfh" id="prefer_wfh" {{ $user->profile->prefer_wfh ? 'checked' : "" }} disabled>
            <label>Prefer WFH?</label>
        </div>
    @endif

    <div class="form-group form-check">
        <input type="checkbox" class="form-check-input" name="is_recruiter" id="is_recruiter" {{ $user->profile->is_recruiter ? 'checked' : "" }} disabled>
        <label>Is Recruiter?</label>
    </div>
    @if( request()->path() == 'profile' )
        <a href="{{ route('edit_profile', [$user->id]) }}"> <input type="submit" name="Edit details" value="Edit details" class="btn btn-dark btn-block"> </a>
    @endif
</div>
</br>

@endsection